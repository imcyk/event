class Event {

  constructor() {

  }

  on(name, self, callback) {
    const tuple = [self, callback];
    if (!global.events) {
      global.events = {}
    }
    const callbacks = global.events[name];
    if (Array.isArray(callbacks)) {
      callbacks.push(tuple);
    } else {
      global.events[name] = [tuple];
    }
  }

  remove(name, self) {
    const callbacks = global.events[name];
    if (Array.isArray(callbacks)) {
      global.events[name] = callbacks.filter((tuple) => {
        return tuple[0] != self;
      })
    }
  }

  emit(name, data) {
    const callbacks = global.events[name];
    if (Array.isArray(callbacks)) {
      callbacks.map((tuple) => {
          try {
              const self = tuple[0];
              const callback = tuple[1];
              callback.call(self, data);
          }
          catch (err) {
            console.log('你是不是把on方法写成了emit方法了，检查下！！')
        }
      })
    }
  }
}

module.exports = Event;